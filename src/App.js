import { PostsList } from './components/PostsList'
import { PostItem } from './components/PostItem'
import { Header } from './components/Header'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import { RecursionWrap } from './components/RecursionWrap'

function App() {
  return (
    <div className="App">
      <Router>
        <div  className="column">
          <Header />
          <Switch>
            <Route path="/post/:postId">
              <PostItem  />
            </Route>
            <Route path="/recursion">
              <RecursionWrap />
            </Route>
            <Route exact path="/">
              <PostsList />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App
