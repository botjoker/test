import { Recursion } from './Recursion'

export const RecursionWrap = (props) => {

  const items = [
    { 
      id: 1,
      title: 'Elements',
      items: [
        {
          id: 2,
          title: '1',
          text: 'This is First one.',
          items: [
            {
              id: 3,
              title: '1:1',
              text: 'This is First of the First one.',
              items: [
                { id: 5, title: '1:1:1', text: 'This is First of the First of the First one.' },
                { id: 6, title: '1:1:2', text: 'This is Second of the First of the First one.', }
              ]
            },
            {
              id: 4,
              title: '1:2',
              text: 'This is Second of the First one.',
              items: [
                { id: 7, title: '1:2:1', text: 'This is First of the Second of the First one.' },
                { id: 8, title: '1:2:2', text: 'This is Second of the Second of the First one.' },
                { id: 9, title: '1:2:3', text: 'This is Third of the Second of the First one.' },
              ]
            },
          ]
        },
        {
          id: 10,
          title: '2',
          text: 'This is Second one.',
          items: [
            {
              id: 11,
              title: '2:1',
              text: 'This is First of the Second one.',
              items: [
                { id: 12, title: '2:1:1', text: 'This is First of the First of the Second one.' },
                { id: 13, title: '2:1:2', text: 'This is Second of the First of the Second one.' }
              ]
            }
          ]
        },
        {
          id: 14,
          title: '3',
          text: 'This is Third one.'
        }
      ]
    }
  ]

  return (
    <>
      <Recursion children = {items} />
    </>
  )

}
