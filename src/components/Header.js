import React from 'react'
import { Button, Navbar, Alignment } from "@blueprintjs/core"
import {Link} from "react-router-dom"

export const Header = () => {
  return(
    <Navbar>
      <Navbar.Group align={Alignment.LEFT}>
          <Link className="pt-button bp3-minimal"  text="Home" to="/">
            <Button className="bp3-minimal" icon="home" text="Home" />
          </Link>
          <Link to="/recursion">
            <Button className="bp3-minimal" icon="document" text="Recursion" />
          </Link>
      </Navbar.Group>
    </Navbar>
  )
}