import React from 'react'

export const Recursion = (props) => {

  return (
    <>
      {props.children.map((item) => {
          if (typeof item.items == 'object') {        
            return (
              <span className="recursion__item" key={item.id}>
                <h2>{item.title}</h2>
                <p>{item.text}</p>
                <span className="recursion__container">
                  <Recursion children={item.items} />
                </span>
              </span>
            )
          } else {
            return (<span className="recursion__item" key={item.id}><h2>{item.title}</h2> <p>{item.text}</p> </span> )
          }    
        })
      }
    </>
  )
}  