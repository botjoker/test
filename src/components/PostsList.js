import React, { useEffect, useState } from 'react'
import { Spinner , InputGroup } from "@blueprintjs/core"
import { Link } from "react-router-dom"

export const PostsList = () => {
  const axios = require('axios')
  
  const [posts, setPosts] = useState([])
  const [searchResults, setSearchResults] = useState([])
  const [searchQuery, setSearchQuery] = useState([])
  const [loading, setLoading] = useState(true)

  const handleChange = event => {
    setSearchQuery(event.target.value)
  }

  useEffect(() => {
    const fetchPosts = () => {
      axios.get('https://jsonplaceholder.typicode.com/posts/')
      .then(({ data }) => {
        setPosts(data)
        setSearchResults(data)
      })
      .catch(
        (error) => console.error(error)
      )
      .finally(
        () => setLoading(false)
      )
    }
    fetchPosts()
  }, [])

  useEffect(() => {
    const filteredResults = posts.filter(item =>
      item.title.toLowerCase().includes(searchQuery)
    )
    setSearchResults(filteredResults)
  }, [searchQuery])

  return (
    <>
       <InputGroup
          asyncControl={true}
          large={true}
          leftIcon="filter"
          onChange={handleChange}
          placeholder="Search..."
      />
      {loading ? 
        <Spinner intent="primary" size="50" /> : 
        searchResults.map(function(item, index){
          return (
            <div className="post__item" key={index}>
              <Link to={`/post/${item.id}`}>{item.title}</Link>
            </div>
          )
        })
      }            
    </>
  )
}