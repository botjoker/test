import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Spinner, Card } from "@blueprintjs/core"

export const PostItem = () => {
  const axios = require('axios')
  let { postId } = useParams() 
  
  const [post, setPost] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const fetchPosts = () => {
      axios.get('https://jsonplaceholder.typicode.com/posts/'+postId)
      .then(({ data }) => {
        setPost(data)
      })
      .catch(
        (error) => console.error(error)
      )
      .finally(
        () => setLoading(false)
      )
    }
    fetchPosts()
  }, [])

  const showPost = () => {
    return (
      <>
        <Card interactive={true} elevation="1">
            <h5>{post.title}</h5>
            <p>{post.body}</p>
        </Card>
      </>
    )
  }

  return (
    <>
      {loading ? 
        <Spinner intent="primary" size="50" /> : 
        showPost()
      }    
      
    </>
  )
}